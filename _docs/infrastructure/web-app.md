---
title: Web App
category: Infrastructure
order: 2
---

We wanted a fast, lightweight JavaScript framework/library for our web application. We evaluated the following three options:

1. ReactJS
1. VueJS
1. Ember

After much back and forth between React and Vue, we eventually settled on React due to its more mature and bigger ecosystem. For all packages that we evaluated, from Material Design ([MaterialUI](https://github.com/mui-org/material-ui) vs [Veautify](https://github.com/vuetifyjs/vuetify)) to SSR ([Next.js](https://github.com/zeit/next.js) vs [Nuxt.js](https://github.com/nuxt/nuxt.js)), the React version had more :star: by a factor of 2-3 as well as more activity in Github Pulse. Lastly, it appeared that React had much greater commercial/professional adoption (Facebook, Netflix, Asana, [and hundreds more](https://github.com/facebook/react/wiki/Sites-Using-React)) than Vue.js (primarily Gitlab, aside from small-scale experiments in other organizations).

So we chose React.

## ReactJS
+ {:.pro} Large ecosystem
+ {:.pro} Commercial adoption in Facebook, Airbnb, Netflix, ...
+ {:.pro} Past month: 59 merged pull requests
+ {:.pro} StackOverflow: 90k questions
- {:.con} Routing not included by default

## VueJS
+ {:.pro} Built based on React best practices
+ {:.pro} Community-driven & built
- {:.con} Ecosystem not as large as React
- {:.con} Past month: 0 merged pull requests
- {:.con} StackOverflow: 20k questions

## Ember
+ {:.pro} Ember Data
+ {:.pro} Convention over Configuration approach
- {:.con} Small ecosystem, few maintained extensions
- {:.con} Difficult to find Ember developers
- {:.con} StackOverflow: 20k questions
