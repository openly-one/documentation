---
title: API Framework
category: Infrastructure
order: 1
---

For the API, we knew we wanted something based on Ruby. Why? Because Finn &mdash; our tech guy &mdash; knows and loves Ruby the most. So we chose between the following API frameworks for Ruby (on Rails):

1. Grape
1. JSONAPI::Resources
1. fast_jsonapi
1. jsonapi-rb

~~We settled on Grape because it seemed both mature and feature-rich. See the analysis below.~~

And then GraphQL took our attention and we chose [GraphQL](http://graphql-ruby.org/) instead.

## GraphQL
+ {:.pro} 2.9k stars
+ {:.pro} Actively maintained
+ {:.pro} Supports Graph Query Language
+ {:.pro} Stable Release
+ {:.pro} Used by [Github](https://githubengineering.com/the-github-graphql-api/#open-source) and [Shopify](https://www.graphql.com/articles/graphql-at-shopify)

## Grape
+ {:.pro} 8k stars
+ {:.pro} Actively maintained
+ {:.pro} [Compatible with fast_jsonapi](https://github.com/ruby-grape/grape/issues/1738)
+ {:.pro} [Can be made JSON API compliant](https://www.hark.bz/blog/post/make-graperails-app-json-api-compliant-cheap/)
+ {:.pro} Stable Release
+ {:.pro} [Used by Gitlab](http://www.ruby-grape.org/users/)
- {:.con} No JSON API compliance out of the box

## JSONAPI::Resources
+ {:.pro} JSON API compliant
- {:.con} Last commit in December 2017
- {:.con} Complex code base, lots of magic
- {:.con} Still in beta

## fast_jsonapi
+ {:.pro} Actively maintained
+ {:.pro} Used by Netflix
- {:.con} No deserialization (not an API framework)

## jsonapi-rb
- {:.con} Last commit in Fall 2017
- {:.con} 164 stars and 1 fork
