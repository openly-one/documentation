---
title: Repositories
category: Infrastructure
order: 4
---

## OpenlyOne/openly-api
Framework: Rails (API-only)
Core repository

## OpenlyOne/openly-web
Framework: React
One frontend repository (web is only one interface for interacting with API)


Advantages of using two repositories:
+ {:.pro} Clearer separation of code
+ {:.pro} Easier deployment
+ {:.pro} Easier, shorter tests

Disadvantages of using two repositories:
+ {:.con} Cannot test front- and backend in combination
    - Solution: Frontend repositories are responsible for testing against specified version of API (source)
+ {:.con} Risk of deploying incompatible versions of front- and backend
    - Solution: Frontend can only deploy if specified version of API matches deployed version of API
