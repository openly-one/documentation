---
title: GraphQL
category: Infrastructure
order: 3
---

After much back and forth, we decided on building a GraphQL API rather than a standard REST API.

We were convinced to take on the additional challenges that come with building a GraphQL API (such as a relatively young and evolving ecosystem), by remarks such as the following:

> Will GraphQL become the de-facto standard for web services in the future and dethrone REST? Maybe. Some prominent API’s, such as GitHub, have already moved their public-facing API’s to GraphQL. Facebook, of course, has been the pioneer and keeps driving forward in the space.
>   
> In this author’s opinion, if you’re building anything beyond a simple data driven application, GraphQL absolutely deserves your attention.
>
> &mdash; [Eric Nograles](https://www.quora.com/What-are-advantages-and-disadvantages-of-GraphQL-SOAP-and-REST/answer/Eric-Nograles)

## Bye, bye Redux?

When we learned that GraphQL might allow us to skip Redux, we were sold:

> I would argue that for most client-side apps, GraphQL can replace the need for Redux entirely.
>
> &mdash; [How GraphQL replaces Redux](https://hackernoon.com/how-graphql-replaces-redux-3fff8289221d)

> In this article, you’ll learn how we offloaded data fetching management to Apollo, which allowed us to delete nearly 5,000 lines of code. Not only is our application a lot slimmer since switching to Apollo, it’s also more declarative since our components only request the data that they need.
>
> &mdash; [Reducing our Redux code with React Apollo](https://blog.apollographql.com/reducing-our-redux-code-with-react-apollo-5091b9de9c2a)

## Tutorial

Additionally, we were convinced by the following **free** and **open-source** tutorial for learning GraphQL from start to finish: [HowToGraphQL](https://www.howtographql.com/)

## Handling File Uploads

When, at last, we learned that there was even a solution to handle file uploads with GraphQL, the decision was made: [How to manage file uploads in GraphQL mutations using Apollo/Graphene](https://medium.freecodecamp.org/how-to-manage-file-uploads-in-graphql-mutations-using-apollo-graphene-b48ed6a6498c)
