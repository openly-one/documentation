---
title: Directory Structure
category: Web App
order: 1
---

The file layout for our ReactJS web application.

```
src/
+-- index.js
+-- scenes/
    +-- Profiles/
        +-- ProfilesIndexScene.jsx
        +-- ProfilesShowScene.jsx
        +-- ProfilesEditScene.jsx
        +-- components/
            +-- ProjectCardCollection.jsx
            +-- ProjectCard.jsx
    +-- Projects/
        +-- ProjectsNewScene.jsx
        +-- ProjectsShowScene.jsx
        +-- ProjectsEditScene.jsx
    +-- NotFound/
        +-- NotFoundScene.jsx
+-- layouts/
    +-- Default.jsx
    +-- components/
        +-- NavigationMenu.jsx
+-- components/
    +-- Banner.jsx
    +-- ProfilePicture.jsx
    +-- Button.jsx
+-- helpers/
    + RoutingHelper.jsx
+-- services/
    +-- api.js
    +-- store.js
    +-- actions/
        +-- profileActions.js
    +-- reducers/
        +-- rootReducer.js
        +-- profileReducer.js
+-- config/
    +-- routes.js
```

## Folders Explained

#### Layouts

Layouts are reuseable templates for our scenes, such as `DefaultLayout` and `SidebarLayout`. The layouts are responsible for content that appears on every page, such as navigation menus.

We were inspired to use layouts by [@dominik.t](https://medium.com/@dominik.t/complete-guide-to-structuring-large-react-redux-apps-bc91e2136d4c).

## Use clear file names

**Do** as much as possible and reasonable, use globally unique file names.  
Call your reducer `userReducer.js` (not `user.js`) and your actions `userActions.js` (not `user.js`).

**Do not** name your file `index.js`.  
When an error occurs, knowing that it originated in `index.js` will be useless if all files are named `index.js`.

**Do** name your file the same as what you're exporting from it.  
As [@david.gilbertson](https://hackernoon.com/the-100-correct-way-to-structure-a-react-app-or-why-theres-no-such-thing-3ede534ef1ed) says:

> name your file the same as the thing you’re exporting from that file
