---
title: Routing
category: Web App
order: 2
---

## Definitions

Routes are defined in `app/config/routes.js`:

```js
// app/config/routes.js

const routes = [
  { component: 'Profile',     path: '/:profileHandle' },
  { component: 'Project',     path: '/:profileHandle/:projectSlug' },
  { component: 'RootFolder',  path: '/:profileHandle/:projectSlug/files/' },
  { component: 'File',        path: '/:profileHandle/:projectSlug/files/:fileID' },
  { component: 'Folder',      path: '/:profileHandle/:projectSlug/folders/:folderID' },
  { component: 'Revisions',   path: '/:profileHandle/:projectSlug/revisions' }
];

export default routes;
```

A route definition consists of two pieces of information:
- `component`: The scene (component) to be rendered
- `path`: The path of this route  
  Can contain URL parameters, marked by `:` and delimited by `/`, such as
  `:profileHandle` in the snippet above.

## Helpers

The Routing helper at `app/shared/helpers/Routing.jsx` automatically creates a
route helper for each route, that you can call to generate the path or link to
one of your routes.

For example, the `Profile` route defined in the snippet above will allow you to
use the following helper method in one of your scenes or components:

```js
// for example: app/scenes/Project/Project.jsx

import React from 'react';
import Routing from 'helpers/Routing';

class Project extends React.Component {
  profilePath() {
    // Return the path for profile and replace URL parameter :profileHandle
    // with 'alice'
    return Routing.ProfilePath({profileHandle: 'alice'});
  }
}
```

## Catch-All / 404

A catch-all route defined in `app/shared/components/Scene.jsx` catches all requests that are not picked up by any of the defined routes:

```js
{/* catch-all route: 404 page / not found */}
<Route component={this.component('NotFound')} />
```

It renders the NotFound scene (`app/scenes/NotFound/NotFound.jsx`).

## How It Works

1. Routes are defined in `app/config/routes.js`
1. Array of routes is made available through Routing helper (`app/shared/helpers/Routing.jsx`) via `Routing.routes`
1. `App.jsx` renders `<Scene />` (`app/shared/components/Scene.jsx`)
1. Scene calls `Routing.routes` to render each route:

```js
// app/shared/components/Scene.jsx

class Scene extends Component {
  route(route) {
    <Route
      exact
      key={route.component_name}
      path={route.regex_path}
      component={this.component(route.component_name)}
    />
  }
}
```
