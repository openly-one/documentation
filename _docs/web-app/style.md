---
title: Style
category: Web App
order: 100
---

## Binding Event Handlers

We use the experimental public class fields syntax for lean event handler bindings. See [Handling Events on reactjs.org](https://reactjs.org/docs/handling-events.html).

```js
class LoggingButton extends React.Component {
  // This syntax ensures `this` is bound within handleClick.
  // Warning: this is *experimental* syntax.
  handleClick = () => {
    console.log('this is:', this);
  }

  render() {
    return (
      <button onClick={this.handleClick}>
        Click me
      </button>
    );
  }
}
```
